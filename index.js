
//1st Part
let firstNum = parseInt(prompt("Provide a number "));
let secondNum = parseInt(prompt("Provide another number "));

let totalNum = firstNum + secondNum;


if(totalNum >= 10){
	if (totalNum <=19 ){
		let difference = firstNum - secondNum;
		alert(`The difference of two numbers are: ${difference}`);
	} else if (totalNum <= 29) {
		let product = firstNum * secondNum;
		alert(`The product of two numbers are: ${product}`);
	} else if (totalNum >= 30) {
		let quotient = firstNum / secondNum;
		alert(`The quotient of two numbers are: ${quotient}`);
	} 

}	else if (totalNum < 10){
		let sum = firstNum + secondNum;
		console.warn(`The sum of two numbers are: ${sum}`);
	}






//2nd Part
let name = prompt("What is your name? ");
let age = (prompt("What is your age? "));

//using IF ELSE
// if(name == "" || age == ""){
// 	alert(`Are you a time traveller?`);
	
// } else if (name != "" && age != "") {
// 	alert(`Hello ${name}. Your age is ${age}`);
// 	isLegalAge(age);
// }

//using Ternary Operator
let ternaryOp = (name == "" || age == "") ? alert("Are you a time traveller?") : (alert(`Hello ${name}. Your age is ${age}.`),isLegalAge(age));


//3rd part
function isLegalAge(checkAge) {

	if (checkAge >= 18){
	 	alert(`You are of legal age.`)
	 	switch(parseInt(checkAge)){
				case 18:
					alert("You are now allowed to party.");
					break;
				case 21:
					alert("You are now part of the adult society.");
					break;
				case 65:
					alert("We thank you for your contribution to the society.");
					break;
				default:
					alert(`Are you sure you are not an alien?`);
					break;
	}
	}
	else if (parseInt(checkAge) <= 17) {
		alert(`You are not allowed here.`);
	}
}


// ----------------------- TRY-CATCH-FINALLY ERRORS

	try {
		isLegalAged();
	}

	catch(e) {
		console.warn("Error has occured: " + e.message)
	}

	finally {
		alert(`This is trigerred inside - finally {} --- Note: This will always run.`)
		isLegalAge(age)
		//using isLegalAge method to print another message --- giving 21 as age value
		
	}

//
// --- 2.) This will catch any negative age input
	// try {
	// 	let negativeAge = parseInt(age)
	// 	if(negativeAge < 0) {
	// 		throw new SyntaxError("Your age should not be negative. I think you are an alien.")
	// 	}
	// }

	// catch(negativeError) {
	// 	console.warn("Error has occured: " + negativeError.message)
	// 	isLegalAge(21)
	// }

	// finally {
	// 	alert(`This is trigerred inside - finally {} --- Note: This will always run.`)
	// 	//using isLegalAge method to print another message --- giving 21 as age value
		
		
		
	// }




